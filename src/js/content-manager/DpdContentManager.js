import React, {Component} from "react";
import {Tabs} from "../tabs/Tabs";

// komponent DpdContentManager przekazal tablice data jako propsa, komponent
// Credentials odebral: data:this.props.data a nastepnie przekazal do dziecka
// Tabs: data={this.state.data}
class Credentials extends Component {
    constructor(props) {
        super(props);
        this.clickButn = this.clickButn.bind(this);
    };

 
    clickButn = (id) => {
        this.props.clickHandler(id); 
    }


    render() {
        return(
            <div className="stepWrapper">
                <div className="stepLabel">
                    <h2>Step 1</h2>
                    <span className="stepNumber">1</span>
                    <span>What are your DPD tracking credentials?</span>
                </div>

                <div className="tabs-container">
                    <Tabs clicked={this.clickButn} data={this.props.data} currentTabId={this.props.currentTabId} />
                </div>
            </div>
        )
    }
}

class PermissionCheck extends Component {
    render() {
        return(
            <div className="stepWrapper">
                <div className="stepLabel">
                    <h2>Step 2</h2>
                    <span className="stepNumber">2</span>
                    <span>Give REACT permission to retrieve tracking events on your behalf.</span>
                </div>
            </div>
        )
    }
}


class DpdContentManager extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentTabId:0,
            data:[
                {
                    id:1,
                    label:"known",
                    text:"Yes I want"
                },

                {
                    id:2,
                    label:"unknown",
                    text:"No, I do not want"
                }
            ]
        }
    }

    changeClass(id) {
        console.log("Clicked");
        this.setState(
            {
              currentTabId: id
            }
        )
    }

    render() {
        return(
            <div>
                <h1>Parent Component DpdContentManager</h1>
                <Credentials clickHandler={(id) => this.changeClass(id)} data={this.state.data} currentTabId={this.state.currentTabId} />
                {this.state.currentTabId !== 2 ? <PermissionCheck /> : null}
            </div>
        )
    }
}

export default DpdContentManager;