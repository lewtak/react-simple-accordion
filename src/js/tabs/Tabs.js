import React, {Component} from "react";
import classNames from 'classnames';

// Komponent Tabs (dziecko), odbiera propsa od rodzica Credentials
export const Tabs = ( {data, currentTabId, clicked} ) => (
    <div className="tabs">
        {data.map( (tab,index)=>{
            return <button onClick={() => clicked(tab.id)} className={`btn btn-default ${currentTabId === tab.id ? "active" : ""}` }
                key={tab.id}
                
            >{tab.text}</button>
        })}
    </div>

)