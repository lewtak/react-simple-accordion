import React from "react";
import ReactDom from "react-dom";
import Root from "../js/Root";

ReactDom.render(<Root />, document.getElementById("main"));