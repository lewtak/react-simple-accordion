import React from "react";
import {Link} from "react-router-dom";


class AccordionSingleItem extends React.Component {
    render() {
        return(
            <div onClick={this.props.clickHandler} className="panel panel-default">
                <div className="panel-heading" role="tab" id={this.props.user.headerId}>
                    <h4 className="panel-title">

                        <a role="button" href="#collapseOne">
                            {this.props.user.button}
                        </a>
                    </h4>
                </div>
                {/* jezeli true dodaj klase opened, jezeli false usun klase */}
                <div id={this.props.user.textId} className={`panel-collapse ${this.props.collapse ? "opened" : ""}`} role="tabpanel">
                    <div className="panel-body">
                        <p>{this.props.user.text}</p>
                    </div>
                </div>

                
            </div>

            
        )
    }
}

export default AccordionSingleItem;