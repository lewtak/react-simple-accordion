import React from "react";
import classNames from 'classnames';
import AccordionSingleItem from "./AccordionSingleItem";


class AccordionItems extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            collapse: -1
        }
    }

    selectFold(e,user) {
        e.preventDefault();
        // Stala current zwroci klikniety id, gdybysmy zostawili user zwrocony zostalby caly obiekt, state -1 jest dla kliku i odklikniecia z powrotem
       const current = this.state.collapse === user.id ? -1 : user.id;
       console.log(current); //zwroci numer id

       this.setState({
            collapse:current
       });

    }

    // selectFold = foldNum => {
    //     const current = this.state.collapse === foldNum ? -1 : foldNum;
    //     this.setState(() => ({ collapse: current }));
    //   };

    render() {
        return(
            <div className="panel-group" id="accordion" role="tablist">
                {this.props.users.map((user,id) => {
                    return(
                       <AccordionSingleItem 
                            key={`${id}-${user.id}`}
                            user={user}// przekazuje parametr user w postaci propsa do dziecka komponentu
                            clickHandler={(e) => this.selectFold(e,user)}
                            collapse={this.state.collapse === user.id}
                       /> 
                    )
                })}

                
            </div>
        )
    }
}


export default AccordionItems;

//this.state.isCollapsed ? 'collapse': null