import React from "react";
import AccordionItems from "./AccordionItems";
import users from "../../json/users";


class Accordion extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            users:[],

        }
    }

    
    render() {
        return(
            <AccordionItems users={users} />                
        )
    }
}

export default Accordion;