import React from "react";

class Details extends React.Component {

    constructor(props) {
        super(props);

        this.state={
            user:{}
        };
    }

    componentDidMount() {
        
    }

    render() {
        //Destructuring from external JSON file
        const {name,place,date,time} = this.state.user;

        return(
            <div>
                <strong>Name: </strong>{name}<br />
                <strong>Place: </strong>{place}<br />
                <strong>Date: </strong>{date}<br />
                <strong>Time: </strong>{time}<br />
            </div>
        )
    }
}

export default Details;