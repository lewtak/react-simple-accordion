import "../sass/style.scss";
import React from "react";
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';

import {AccordionComponent} from "./accordion/AccordionComponent";
import Details from "./details/Details";

const App = () => {
    return(
        <Router>
            <div>
                <Route path="/" component={AccordionComponent} />
                <Route path="/details/:userId" component={Details} />
            </div>
        </Router>
    )
};

export default App;
