const path = require("path");
const webpack = require("webpack");
const ExtractTextWebpackPlugin = require("extract-text-webpack-plugin"); // plugin aby wyciagnac  z bundle.js style css oraz header npm i -D extract-text-webpack-plugin@next lub npm install --save-dev mini-css-extract-plugin
const HTMLWebpackPlugin = require("html-webpack-plugin");
const HtmlWebpackExternalsPlugin = require("html-webpack-externals-plugin");
// za pomocą tego pluginu dodamy bundle.js oraz main. css dynamicznie do naszego index.html

module.exports = function(env) {
  var prod = env !== undefined && env.production === true; // zmienna ta pozwoli na to aby odniesc sie potem do warunku, czy produkcja czy development
  var dev = env !== undefined && env.development === true;

  //po zapisaniu do funkcji, bede mogl go zwrocic
  return {
    //Entry (od czego webpack ma zacząć kompilować)

    // tworzenie wielu entry pointow dla kilku bundli

    //entry: "./src/js/app.js", taki zapis w przypadku jednego bundla
    entry: {
      react: "./src/js/index.js",
      main:"./src/js/Main.js",
      vendors: [
        "react",
        "react-dom",
        "prop-types",
        "jquery",
        "es6-promise/auto",
        "whatwg-fetch"
      ]
    },

    mode: "none", // mode jest konieczny //https://webpack.js.org/concepts/#mode
    // output - gdzie ma być zapisany docelowy bundle
    // publicPath: "/dist/", niezbędne aby po uruchomieniu servera lokalnego npm
    // start, index.html kierował na katalog dist i stamtąd pobrał ściezkę
    output: {
      publicPath: dev ? "/dist/" : "",
      path: path.resolve(__dirname, "dist/"),
      //filename: "bundle.js"
      filename: prod ? "[name].[chunkhash].js" : "[name].js" //logika polegajaca na tym, ze jezeli pracujemy juz na live server chcemy do plikow dodawac hash, a jak server webpack to bez hash
    },

    //Korzystamy z webpack devtool w celu utworzenia sourcemap, co bedzie pomocne przy debaguwaniu bledow w kodzie
    devtool: prod ? "source-map" : "cheap-module-eval-source-map",

    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: "vendors",
            chunks: "all"
          }
        }
      }
    },

    module: {
      rules: [
        //w rules podajemy rózne obiekty dla poszczególnych loaderów, w pierwszym
        //obiekcie transpilacka do ES15
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["es2015","stage-2", "react"],
              plugins: ["lodash"]
            }
          }
        },
        // {
        //   test: /\.hbs$/,
        //   exclude: /node_modules/,
        //   use: {
        //     loader: "handlebars-loader"
        //   }
        // },
        {
          test: /\.scss$/,
          // use: [
          //   { loader: "style-loader" },
          //   { loader: "css-loader" },
          //   { loader: "sass-loader" }
          // ]
          use: ExtractTextWebpackPlugin.extract({
            fallback: "style-loader",
            use: ["css-loader", "sass-loader"]
          })
        },

        {
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
          use: {
            loader: "url-loader",
            options: {
              limit: 10000,
              name: "img/[name].[ext]"
            }
          }
        }

        //   {
        //     test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        //     use: {
        //       loader: "file-loader"
        //     }
        //   }
      ]
    },

    plugins: [
      new ExtractTextWebpackPlugin("css/main.css"),
      new HTMLWebpackPlugin({
        //template: path.resolve(__dirname, "index.html")
        filename:'index.html',
        template: "./src/index.html",
        chunks: ["vendors", "react"],
        inject: true,
        hash: true
      }),

      new HTMLWebpackPlugin({
        //template: path.resolve(__dirname, "index.html")
        filename:'about.html',
        template: "./src/about.html",
        chunks: ["vendors", "main"],
        inject: true,
        hash: true
      }),

  
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
      }),

      new HtmlWebpackExternalsPlugin({
        externals: [
          {
            module: "bootstrap",
            entry: {
              path:
				"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
				type:"css"
            }
          },

          {
            module: "TweenMax",
            entry: {
              path:
                "https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"
            }
          },

        ]
      })
    ]
  };
}; //end of exports function
